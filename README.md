# Hi there 👋
#### Student at Nizhny Novgorod University

## My tech stack 

<p align="center">
<img src="https://readme-components.vercel.app/api?component=logo&logo=c&text=false&animation=false&fill=3864c3&textfill=white&">
 <img src="https://readme-components.vercel.app/api?component=logo&logo=C%2B%2B&text=false&animation=false&fill=3864c3&textfill=white&">
 <img src="https://readme-components.vercel.app/api?component=logo&logo=CMake&text=false&animation=false&fill=3864c3&textfill=white&"> 
 <img src="https://readme-components.vercel.app/api?component=logo&logo=ArchLinux&text=false&animation=false&fill=3864c3&textfill=white&"> 
 <img src="https://readme-components.vercel.app/api?component=logo&logo=Git&text=false&animation=false&fill=FE5000&textfill=white&">
 <img src="https://readme-components.vercel.app/api?component=logo&logo=oracle&text=false&animation=false&fill=red&textfill=white&">
 <img src="https://readme-components.vercel.app/api?component=logo&logo=Vim&text=false&animation=false&fill=green&textfill=white&">
 <img src="https://readme-components.vercel.app/api?component=logo&logo=Neovim&text=false&animation=false&fill=green&textfill=white&">
 <img src="https://readme-components.vercel.app/api?component=logo&logo=GNUbash&text=false&animation=false&fill=black&textfill=white&"> 
</p>

## Contact
<p align="center"><a href="https://t.me/QapFUc" target="blank">  <img src="https://readme-components.vercel.app/api?component=logo&logo=Telegram&text=false&animation=false&fill=3864c3&textfill=white&"></a>
<a href="mailto:qapfuc@gmail.com" target="blank"> <img src="https://readme-components.vercel.app/api?component=logo&logo=GMail&text=false&animation=false&fill=red&textfill=&"></a>
<a href="https://www.linkedin.com/in/dmitriy-usov-90858525a/" target="blank"> <img src="https://readme-components.vercel.app/api?component=logo&logo=Linkedin&text=false&animation=false&fill=3864c3&textfill=white&"></a>
<a href="" target="blank"> <img src="https://readme-components.vercel.app/api?component=logo&logo=HellyHansen&text=false&animation=false&fill=red&textfill=white&"></a></p>
